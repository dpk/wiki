#+TITLE: Data storage

- [[file:portable-pickle.org][Portable Pickle]]
- [[file:safe-archive.org][Safe archive]]
- [[file:suffix-array.org][Suffix array]]
- [[file:fm-index.org][FM-index]]
