#+TITLE: Compartmentalization

Now that I have this wiki, and my website, and Twitter, and Swhack, it's
not entirely clear where I should publish what.

- My website is probably to be kept for /articles,/ of exactly the kind
  which I already write with insufficient frequency.
- This wiki is for rough notes, ill-thought-out ideas, advice.
- Swhack is for thinking out loud and sharing links.
- Twitter can be almost entirely disregarded. Twitter is stupid (cf.
  [[http://swhack.com/logs/2015-05-25#T02-22-11][discussion with
  Kragen]]).

One way to look at it is:

- My website is a set of immutable articles (I very rarely modify pages
  once they've been published there --- I think only two of them have
  been changed since I published them).
- My wiki is a set of mutable articles.
- Swhack is an immutable (append-only) scroll.
- There is no mutable scroll. If I had one, I might call it a 'blog',
  though I don't really see the value in such a thing.
