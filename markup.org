#+TITLE: Markup

- [[file:microxml.org][MicroXML]]

A standard typology of markup systems is presentational, procedural,
and descriptive (also sometimes unhelpfully called semantic). Frank W.
Tompa uses this, for example, in his [[https://www.digitalstudies.org/articles/10.16995/dscn.224/][article introducing the /OED/
database software developed at Waterloo]].

However, while descriptive vs presentational is a reasonable dichotomy
(or perhaps better said, spectrum), descriptive vs /procedural/ markup
is a completely false dichotomy. This was explored in detail by
Wendell Piez in [[https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.139.978&rep=rep1&type=pdf][‘Beyond the “descriptive vs. procedural” distinction’]].
