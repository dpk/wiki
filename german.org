#+TITLE: German

[[file:img/german.png]]

A West Germanic language spoken mainly in Germany, Austria, and
Switzerland.

I have a few [[file:favourite-german-words.org][favourite words in German.]]

** Prepositions
   :PROPERTIES:
   :CUSTOM_ID: prepositions
   :END:

- "Durch, für, gegen, ohne, wider, um / kennst du nicht dein Akkusativ,
  dann bist du wirklich dumm!"
- "Aus, bei, mit, von, nach, seit, zu / kennst du nicht dein Dativ, dann
  bist du eine Kuh!"
- An, auf, hinter, in, neben, über, unter, vor, zwischen take dative for
  location, accusative for motion
- statt, trotz, während, wegen: notionally genitive, aber hier ist der
  Dativ dem Genitiv sein Tod.
- außerhalb, innerhalb: again, notionally genitive, but you can add
  'von' and use the dative colloquially, too.

** Gender tips
   :PROPERTIES:
   :CUSTOM_ID: gender-tips
   :END:

- -e often means "feminine", -er often means "masculine" (except when
  they mean "plural", but if you don't know whether you're talking about
  one or many, you've got other problems)
- -chen/-lein is always neuter
- -ung is always feminine

** Inflexion tips
   :PROPERTIES:
   :CUSTOM_ID: inflexion-tips
   :END:

- If you think of definite articles as being the "strongest" class of
  articles, no article at all being the "weakest", and indefinite
  articles being the middle: the declension strength of adjectives is
  always the inverse of the strength of article. You might think this
  makes things more confusing, but I find it quite a useful mnemonic. 

** Verb tips
   :PROPERTIES:
   :CUSTOM_ID: verb-tips
   :END:

- Verbs whose infinitives begin with ge- (almost?) exclusively take the
  dative case for their objects.
