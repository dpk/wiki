#+TITLE: My Secret Life

The world's longest autobiography; probably the world's longest
pornographic book, too. Who wrote it? [[file:walter.org][Nobody
knows.]] When was it published? Roughly 1888--1894, but the earliest
definite record of its existence isn't until 1902. Its first edition was
of 11 volumes, probably issued over the course of several years, but
nobody knows when each volume came out. I have some unpublished research
on the dating which I'll hopefully get around to sharing one day.
