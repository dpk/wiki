#+TITLE: Naming things

#+begin_quote
There are only two hard things in Computer Science: cache invalidation and naming things.
#+end_quote

— [[https://skeptics.stackexchange.com/questions/19836/has-phil-karlton-ever-said-there-are-only-two-hard-things-in-computer-science][probably]] Phil Karlton

Some people complain about systems which adopt idiosyncractic names for things. ~car~ and ~cdr~ (and sometimes even ~cons~) in Lisp are frequent targets. [[https://aty.sdsu.edu/bibliog/latex/gripe.html][Here’s someone complaining about TeX’s ‘overly-cute jargon, like “glue”.’]]

The core of the problem Karlton refers to in the quote above is that the English language often applies the same word to things which would definitely be better modelled as very distinct entities in a computer system. My favourite example is a booking system for train tickets. A ‘train’ could be:

- A route between two locations. (‘The train from London to Manchester’ says nothing about when it will run; it might not even be a direct service.)
- A timetabled entry for a service on such a route. (‘The weekday train from London to Manchester which leaves at 09.30 and arrives at 11.45’ refers to many trains running regularly for an indefinite period of time.)
- A particular instance of one of these services, usually on a particular calendar day. (‘The train from London to Manchester leaving at 09.30 and arriving at 11.45 on 3 February’ refers to one train and tells you exactly where and when it goes.)
- A timetable entry for a service at one particular station. (‘The weekday train from London, which arrives at Milton Keynes at 10.00 and leaves Milton Keynes at 10.05 going to Manchester’)
- A particular instance of such a timetable entry at one station.
- One physical locomotive. (Okay, you’re unlikely to need this for a ticket booking system, but a complete system for managing the whole of a railway system would need it.)
- A class of locomotive.
- A physical locomotive connected to multiple physical wagons.
- A class of locomotive connected to wagons of multiple classes.
- A combination of classes of wagons associated with a particular timetabled service (for a booking system you’d need to know this to know the planned seat layout in the train to let people make seat reservations).
- Probably more!

If you’re writing a program that deals with trains and train timetables in any way, it makes sense to give each of these meanings of ‘train’ a different name and maybe even not to give the name ‘train’ to any of them.

In the case of TeX’s glue, the term ‘space’ has multiple meanings in typography: it could be a space /character/ (of any of several types — non-breaking, thin, etc.), for example. Andrew Young suggests ‘leading’, but this is too specific, referring in classical typography only to strips of lead inserted between lines to increase the spacing. It makes sense to invent an idiosyncratic term for the concept as it is idiosyncratically understood by TeX.

In developing [[https://greensdictofslang.com][/Green’s Dictionary of Slang/ Online,]] I had a similar problem about the word ‘citation’. In most historical dictionaries, quotations consist of a citation and a text, but /GDoS/ uses ‘citation’ to refer to both the quotation and the citation (in the terms of other dictionaries). To match citations (in the common meaning) to the bibliography, I created a data structure initially called a ‘matched citation’, but I later called it a ‘stencil’ in reference to the terminology used by the /Middle English Dictionary/ (and occasionally, but rarely, by others).
