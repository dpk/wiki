#+TITLE: Open Access

Open Access is the most important cross-disciplinary movement in
academia right now.

Mike Taylor suggests that authors should
[[http://svpow.com/2013/05/13/who-owns-a-peer-reviewed-revised-accepted-manuscript-you-do/][take
advantage of the gap between acceptance of a paper and copyright
reassignment]] to allow researchers to publish in journals which have
copyright transfer requirements and don't allow open access. In that gap
you can release to the [[file:public-domain.org][public domain]], which is
irrevocable and voids any subsequent copyright transfer signing, or
spread it as far and wide as you can under a Creative Commons licence.
Unfortunately if many people do this, they may well close the gap by
e.g. making authors sign a provisional transfer before the peer review
stage.

Taylor has also used the trick of
[[http://svpow.com/2010/10/13/who-owns-my-sauropod-history-paper/][transferring
copyright to his wife]] before publication so that the copyright is not
his to transfer.
