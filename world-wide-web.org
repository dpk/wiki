#+TITLE: World Wide Web

A global information network. [[file:wiki.org][Wikis]] are closer in spirit
to the original idea of the Web than any other kind of site that's
popular today. Today's Web is more of a global application network than
an information network.

- [[file:peak-web.org][Peak Web]]
- [[file:css.org][CSS]]
- [[file:ccss.org][CCSS]], Constraint Cascading Stylesheets
- [[file:web-history.org][Web history]]
