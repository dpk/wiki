#+TITLE: Spelling reform

Spelling reform is something that English badly needs.

** Process
   :PROPERTIES:
   :CUSTOM_ID: process
   :END:

A quick look at the successes of spelling reforms in German (1901, 1996)
will show one thing that's needed if a spelling reform is to have
success: it must not change too many things. In particular, it must not
change the basic spelling-to-sound rules of the existing system, just
tweak how they're applied. Specifically, reforms which don't use the
Latin alphabet, or which add new characters to it, though they might
have worked if instigated 50 years ago, are now completely sunk since
they can't be used on today's English computer keyboards. Adopting a
spelling reform shouldn't require buying a new keyboard.

One good example of a proposal which keeps within this rule is the "drop
useless 'e's" proposal made by the Spelling Society, though I would add
two codicils to their original: it should only apply to the magic "e",
at least initially, and they can be kept in cases where the root vowels
are already lengthened in a digraph, unless there is a good etymological
reason (with analogy to another common word). So "perceive", "sleeve",
and "valley" can keep their "e"s, but "therefore" should become
"therefor" and "have" will be respelt as "hav". This is quite a dramatic
change, but the 1996 German reform was quite successful at changing the
common word "daß" to "dass". Also, "Stage 1" in its entirety is probably
too dramatic.

As such, I think the best way to achieve a spelling reform is first of
all to gather a group of influential people who'll agree to adopt it ---
if the government's education department is included, so much the better
--- then follow this three-stage programme:

1. Produce a complete, systematic description of the current English
   spelling rules. I call this an
   [[file:english-rechtschreibung.org][English Rechtschreibung]].
2. Call for proposals to improve the spelling system as described.
   Decide which ones are viable to implement, and produce a description
   and comprehensive wordlist of reformed spellings.
3. Get the group to start using it, and proselytize it.

The
[[http://spellingsociety.org/international-english-spelling-congress][International
Spelling Congress]] is a good start, though they're starting at stage 2.
I believe that the only way a spelling reform will succeed is with a set
of well-described rules behind it which encompass the existing words of
the language, as well as the reformed words.

A useful intermediate step for some words could be a "compromise form"
which retains vestiges of the older spelling. For instance, if "though"
is to be respelt as "tho", an intermediate form could include an
apostrophe at the end, as [[file:eric-gill.org][Eric Gill]] spelt it. (In
Scots they call this an
[[https://sco.wikipedia.org/wiki/Apologetic_apostrophe][apologetic
apostrophe]].) The apostrophe would inevitably disappear over time as
people forget the old spelling altogether.

** Opposition
   :PROPERTIES:
   :CUSTOM_ID: opposition
   :END:

*** The descriptivism argument
    :PROPERTIES:
    :CUSTOM_ID: the-descriptivism-argument
    :END:

The most sophisticated of the arguments, this argument posits that
language should be described rather than prescribed, and that spelling
is a part of language.

The problem with this argument is that spelling is, by its very nature,
prescribed: unlike speaking and listening to the language, reading and
writing has to be taught explicitly. The language we should aim to
/describe/ in linguistics is the spoken language.

*** The conservative argument
    :PROPERTIES:
    :CUSTOM_ID: the-conservative-argument
    :END:

"I like English spelt the way it is now." This is one of the hardest
arguments to break, and the unspoken thought underlying many of the
other arguments.

Spelling reform is not for the benefit of those who can already read and
write. It is for the benefit of those who need to learn to read and
write: children, people learning English and a foreign language, and
illiterate adults.

*** The aesthetic argument
    :PROPERTIES:
    :CUSTOM_ID: the-aesthetic-argument
    :END:

Similar to the conservative argument, some people are okay with the
/idea/ of spelling reform, but when they see the results they react
badly. I even fall into this trap when I see texts written in reformed
spelling.

There's not much to be done about this except familiarize yourself with
new spellings and get used to them. For instance, when I was considering
the "drop useless 'e's" reform, I found some examples quite ugly, but
adjectives ending in -ive ("dativ" for "dative", "activ" for "active",
etc.) I found okay. Then I realized it was because I was already
familiar with many of those spellings from German.

*** The etymological argument
    :PROPERTIES:
    :CUSTOM_ID: the-etymological-argument
    :END:

Some people argue that English spelling is good because it shows the
etymology of words as well as their pronunciation.

Like in "could", for instance. Or "ache".  In truth, English spelling
doesn"t do this either consistently or very well. It"s also a
distraction: spelling should not be an "old curiosity shop" of the
orthographies of languages from which the language has borrowed words.

*** The loanword argument
    :PROPERTIES:
    :CUSTOM_ID: the-loanword-argument
    :END:

Related to the etymological argument, English borrows lots of words from
other languages with other orthographies, which some claim will not
conform to the spelling system when they're borrowed, obviating the
entire point of a consistent orthography.

The example of German shows this to be untrue: many German words from
other languages have had their spelling naturalized --- in most cases,
not by prescriptive application of a new spelling, but simply by the
passage of time and their gradual acceptance into the language.

*** The semantic argument
    :PROPERTIES:
    :CUSTOM_ID: the-semantic-argument
    :END:

Some reforms propose to merge the spellings of certain homophones, which
opponents claim will cause meanings to get confused.

But the meanings don't get confused in speech, so they won't be in
writing either. Besides, a spelling reform needn't necessarily merge
all, or even most homophones into one spelling. The orthographic system
of French, for instance, is extremely consistent in its mapping of
spellings to sounds, in the sense that almost every spelling has only a
single sound, but many sounds have lots of spellings.

*** The literary argument
    :PROPERTIES:
    :CUSTOM_ID: the-literary-argument
    :END:

Related to the conservative argument, some people oppose English
spelling because future generations won't get to enjoy the works of
Shakespeare and Dickens in their original spelling.

Most people who say this have probably never read Shakespeare in its
"original spelling" either. The editors of modern editions change the
spellings to modern ones --- the originals, in 17th century quartos and
folios, are much different. Spelling has changed even since the time of
Dickens, with "today" now being preferred over "to-day", for instance.
It doesn't destroy the literature to have to have it rendered in new
spelling.

*** The dialect argument
    :PROPERTIES:
    :CUSTOM_ID: the-dialect-argument
    :END:

One sometimes sees the claim that there are too many dialects, and too
much regional variation in the pronunciation of English, for spelling
reform to provide a useful system for all speakers of the language.

English spelling is already a decently diaphonemic system. As long as
reformers were careful not to damage any existing dialect cues in
spelling (which would be easy with an English Rechtschreibung as the
basis of the reform). There are many such changes that can be made that
do not damage pronunciation: in no dialect of English is the word 'have'
pronounced like it's currently spelt, for instance. John Wells of the
Spelling Society, also a highly regarded phonetician and pronunciation
dialectologist, wrote a
[[http://www.phon.ucl.ac.uk/home/wells/accents_spellingreform.htm][short
paper]] on the implications of accent variation on spelling reform,
concluding that the problems posted are surmountable.

*** The economic argument
    :PROPERTIES:
    :CUSTOM_ID: the-economic-argument
    :END:

The proponents of this argument claim that English spelling would cost
too much to change.

What they miss is the tremendous economic /benefit/ of the change: more
people will be able to learn English, and do business in English with
English-speaking countries. If the spelling change is kept reasonably
small, the cost of changing is still minimal. The main cost is in
education: school textbooks need to be changed to use the new spelling,
but the only important ones to change immediately are the spelling books
themselves. Books in other fields like mathematics which use the old
spelling could be kept for as long as they'd ordinarily be useful.

The other cost is in educating the public in the new spellings and
encouraging them to use them. Those who work in schools teaching
spelling, or for the government (or any organization which has adopted
the reforms) will have to know how to use the new system. Both these
immediate costs are fairly minimal.
