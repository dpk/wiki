#+TITLE: Lexicography

#+BEGIN_QUOTE
  Lexicography can be done on the kitchen table.
#+END_QUOTE

--- Charles Talbut Onions, as quoted by Robert Burchfield.
([[http://www.independent.co.uk/news/obituaries/robert-burchfield-38804.html][Source]])

Lexicography is the art, craft, and science of compiling dictionaries.
In broader use, it's also applied to related activities like finding
quotations for a historical or unabridged dictionary, and to the
creation of other reference works like encyclopædias and thesauruses.

The above quotation is one of my favourites on the craft. I also find
[[http://lasersoptional.com/wp-content/uploads/2014/01/Screen-Shot-2014-01-15-at-5.11.29-PM.png][this]]
matches my experience closely.

Lexicography, especially historical lexicography, takes a long time.
Even the 44 years that elapsed between the publication of the first
fascicle of the /OED/ and the completion of the last volume looks
insignificant compared to the 123 years which the scholars working on
the Grimm /Deutsches Wörterbuch/ spent labouring over the history of
Modern High and Standard German.

And it does not get appreciably quicker: the /OED/ editors have decided,
explicitly or not, to use the speed-ups granted by the use of computers
to do more in the same amount of time, rather than to do the same amount
in less time. The /OED/'s third edition will take about forty years at
the current rate --- about the same amount of time the original edition
took --- but will be about double the size when it's done.

Dictionaries are examples of large and complex
[[file:structured-text.org][structured texts]].

My current interests in lexicography are currently mainly centred around
/Green's Dictionary of Slang/.
