#+TITLE: Wiki

Wikis are great. They're underused. There are basically two kinds of
wiki: personal wikis (like this one) and Wikipedia.

- [[file:wiki-building.org][Wiki-building]]

[t] and [k] are allophones in free variation in Hawaiian: 'wiki' could
equally be pronounced /ˈwɪti/.
