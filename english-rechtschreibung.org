#+TITLE: English Rechtschreibung

An English Rechtschreibung --- a book systematically, descriptively
laying out the current spelling-to-sound rules of English, noting all
the exceptions --- is, in my view, the first reasonable step towards
English [[file:spelling-reform.org][spelling reform]]. It is also the
easiest step. Once the Rechtschreibung is done, the next step is to
decide on what reforms to introduce that will simplify it, and then, in
the hardest step of all, to get the reform implemented.

It would not be enough to simply study the spellings of all words in
English. Native English speakers, coping with the
[[http://spellingsociety.org/irregularities-of-english-spelling][insanity]] that
is our orthographic "system", do have something of an internal,
presumably consistent, spelling-to-sound model which we use to guess the
spelling of unfamiliar words. This comes from a mix of the "taught
rules" of spelling we learn in primary school, and also from the
experience of reading and hearing the written word spoken aloud  and
thus acquiring the more subtle aspects of it (such that there are) by
osmosis.

In order to make this model an object of study for the Rechtschreibung
--- so that reforms can be designed to fit within existing speakers'
conceptions of spelling--sound correspondences --- a large group of test
subjects, with a variety of educational, national, and cultural
backgrounds, should be asked to read aloud nonsense words which are
created to test the hypotheses the Rechtschreibung puts forward about
rules. For instance, if the book claims that /ɡ/ is the standard
realization of the written letter ‹g› and that the 'soft' /dʒ/ is an
exception, a variety of nonsense words employing ‹g› in various contexts
should be used to find out how native speakers guess which pronunciation
to use.
