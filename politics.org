#+TITLE: Politics

** Democratic systems

- [[file:liquid-democracy.org][Liquid democracy]]
- [[file:voting-systems.org][Voting systems]]

** Software politics and law

- [[file:software-licences.org][Software licences]]

** United Kingdom

- [[file:house-of-lords.org][House of Lords]]
