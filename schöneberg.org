#+TITLE: Schöneberg

My area of residence in Berlin from May 2014 to roughly January 2015.
(On New Year's Day 2015 I was forced out of my home, but stayed with
friends in the area for an extra two weeks before temporarily returning
home to the UK.)

Schöneberg is a fairly quiet area of the city. It has its own interest,
though.

- The Volkspark Schöneberg is long and snaking and stretches all the way
  from Martin-Luther-Straße to Hohenzollerndamm in Wilmersdorf.
- Viktoria-Luise-Platz is a preserved historic square with buildings,
  lamps, and street signs from the 1920s.
- Around Nollendorfplatz is the (historic, yet still vibrant) centre of
  Berlin's gay community. The first memorial to the gay victims of the
  Holocaust is outside the U-Bahnhof.
- KaDeWe is famous but mostly boringly commercial. But the top floor is
  wonderful. (I have not tried the restaurant in the roof.)
- There's often interesting things happening in the squares around the
  Kaiser-Wilhelm-Gedächtniskirche.
- Berlin's only 24-hour supermarket (Monday--Friday) is at Berliner
  Straße 24.
