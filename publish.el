;; publish.el --- Publish org-mode project on Gitlab Pages
;; Author: Rasmus
;;; Commentary:
;; This script will convert the org-mode files in this directory into
;; html.
;;; Code:
(require 'package)
(package-initialize)
;;(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'org)
(package-install 'org-contrib)
(package-install 'htmlize)

(require 'org)
(require 'ox-publish)

(push
 '("en-GB"
   (primary-opening :utf-8 "‘" :html "&lsquo;" :latex "`" :texinfo "`")
   (primary-closing :utf-8 "’" :html "&rsquo;" :latex "'" :texinfo "'")
   (secondary-opening :utf-8 "“" :html "&ldquo;" :latex "``" :texinfo "``")
   (secondary-closing :utf-8 "”" :html "&rdquo;" :latex "''" :texinfo "''")
   (apostrophe :utf-8 "’" :html "&rsquo;"))
 org-export-smart-quotes-alist)

(defun org-export-git-date (ogdf info &optional fmt)
  (let* ((fmt (or fmt org-export-date-timestamp-format))
         (org-date (funcall ogdf info fmt)))
    (if (and (null org-date) fmt)
        (let ((git-lastmod
               (with-output-to-string
                 (with-current-buffer standard-output
                   (call-process
                    "git"
                    nil t nil
                    "log" "-1" "--pretty=format:%aI" "--since=Wed Mar 10 11:18:48 2021 +0100" "--"
                    (plist-get info :input-file))))))
          (if (not (string-blank-p git-lastmod))
              (format-time-string
               fmt
               (encode-time (iso8601-parse git-lastmod)))
            nil)))))
(advice-add 'org-export-get-date :around #'org-export-git-date)

(defun org-links-graph (project)
  "Construct a graph of all internal links in project."
  (let ((root (expand-file-name
	       (file-name-as-directory
		(org-publish-property :base-directory project))))
        (links-graph nil))
    (dolist (file (org-publish-get-base-files project))
      (add-to-list 'links-graph
                   (cons (org-publish-file-relative-name file `(:base-directory ,root))
                         (org-get-outbound-links file project))))
    links-graph))

(defun org-get-outbound-links (file project)
  "Get a list of all internal links leading outbound from file in
project."
  (let ((file (org-publish--expand-file-name file project)))
    (when (and (file-readable-p file) (not (directory-name-p file)))
      (let* ((org-inhibit-startup t)
	     (visiting (find-buffer-visiting file))
	     (buffer (or visiting (find-file-noselect file)))
             (outlinks nil))
	(unwind-protect
            (with-current-buffer buffer
              (while (and (org-next-link)
                          (not org-link--search-failed))
                (let ((this-link (org-element-link-parser)))
                  (if (string= (org-element-property :type this-link) "file")
                      (add-to-list 'outlinks this-link)))))
          (unless visiting (kill-buffer buffer)))
        outlinks))))

(setq user-full-name "Daphne Preston-Kendal")
(setq org-export-with-section-numbers nil
      org-export-default-language "en-GB"
      org-export-with-smart-quotes t
      org-export-time-stamp-file nil
      org-export-with-date t
      org-export-date-timestamp-format "%e %B %Y"
      org-export-with-toc nil)
(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%e %B %Y"
      org-html-link-home "/"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-doctype "html5")
(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")
(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "."
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public"
             :exclude (regexp-opt '("README" "draft"))
             :auto-sitemap t
             :sitemap-file-entry-format "%d *%t*"
             :html-head-extra "<link rel=\"stylesheet\" href=\"/css/org.css\" >"
             :sitemap-style 'list
             :sitemap-sort-files 'anti-chronologically)
       (list "site-static"
             :base-directory "."
             :exclude "public/"
             :base-extension site-attachments
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment
             :recursive t)
       (list "site" :components '("site-org"))))
(provide 'publish)
;;; publish.el ends here
