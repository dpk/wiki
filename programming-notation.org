#+TITLE: Programming notation

/Notation/ is the traditional Lisp name for what most languages call
syntax. It's a better name. Notation implies flexibility, expressivity,
and also admits the arbitrariness of the choices made by the language
designer in fixing the notation.

/Notation/ is a mathematical term, also. Mathematical notation is very
powerful because it uses whatever special characters it needs to --- and
mathematicians can add their own symbolisms when they need them --- and
takes significantly better advantage of two dimensions than typical
programming language notation does. (Summations and integrals are good,
simple examples; matrices also.) It also, perhaps more relevantly, makes
good use of typographical effects: bold, italics, superscript,
subscript, even changing Schriftart to sans-serif or even Fraktur to
make distinctions. Programming languages haven't done anything like this
since ALGOL 60; even there,
[[https://en.wikipedia.org/wiki/Stropping_(syntax)][stropping]] was
needed to replicate the effects in plain text. I would use the term
'rich notation' to describe a programming language which uses notation
this effectively.

- Kragen Sitaker: /[[http://canonical.org/~kragen/sw/dev3/paperalgo][A
  paper algorithm notation]]/
- [[http://swhack.com/logs/2015-06-09#T07-45-55][Chat with Kragen about
  the above]]
