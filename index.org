#+TITLE: Homepage

Welcome to [[http://dpk.io/][my]] personal wiki. Here I'm collecting notes, tips, random thoughts, and other ephemery. [[file:compartmentalization.org][I'm still working out the difference between this wiki and my main site.]] Here are some places to start exploring the wiki:

- [[file:berlin.org][Berlin]]
- [[file:language.org][Language]]
- [[file:computing.org][Computing]]
- [[file:politics.org][Politics]]
- [[file:christianity.org][Christianity]]
- [[file:sexuality.org][Sexuality]]
- [[file:polyamory.org][Polyamory]]
- [[file:ideas.org][Ideas]]
- [[file:miscellaneous.org][Miscellaneous]]
- or just [[file:sitemap.org][see a list of all the pages]]

Note that I’m currently in the process of moving this wiki over from an old system I wrote myself to one based on [[file:org-mode.org][org-mode]] in [[file:emacs.org][Emacs]]. Most of the content here hasn’t been updated since 2015! Pages which are severely out of date (i.e. which haven’t been touched since 2015) don’t have a date at the bottom; pages which I’ve updated since have the date I last changed something on that page. Even then, I may just have fixed a broken link or something on that date, and not have got round to updating the content yet.

This wiki was inspired by the personal wikis of Sean B. Palmer (not public and now offline) and Miki Mokrysz (also now offline). Their personal wikis were in turn inspired by the personal wikis of Dan Connolly (/also/ now offline, sigh) and [[http://people.csail.mit.edu/mrub/notes/doku.php][Michael Rubinstein]], respectively.
