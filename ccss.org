#+TITLE: CCSS

Constraint Cascading Stylesheets, first [[http://citeseer.ist.psu.edu/viewdoc/download;jsessionid=43F71E42AAADE8C0AF995CC56A5070BB?doi=10.1.1.101.4819&rep=rep1&type=pdf][proposed]] by Greg Badros [[file:peak-web.org][in 1999]]. Unfortunately they were never implemented by browsers.

Around 2015 there was a JavaScript implementation of CCSS available, called [[http://gss.github.io/][Grid Stylesheets]] (not to be confused with the later [[https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout][CSS Grid Layout]]). Unfortunately it no longer seems to be maintained.
