#+TITLE: Patch theory

The Darcs DVCS attempted to create a 'patch algebra' to form a
mathematically-sound theoretical underpinning for its operation.
[[http://www.cs.tufts.edu/comp/150GIT/archive/iago-abal/mfes_darcs.pdf][Unfortunately
it was never quite mathematically sound.]]

The best way to view DVCSes like Git from a theoretical perspective, it
seems, is to see them as a directed [[file:Graph_theory][graph]] where
the commits are nodes and the edges are diffs between those nodes. Most
nodes have only one edge connecting to them --- their parent commit ---
but some, merge commits, will have more. To my knowledge, this theory
was first described by Sam Livingston-Gray in his book
/[[http://think-like-a-git.net/][Think Like (a) Git.]]/
