#+TITLE: R7RS Ultraviolet issues

#+TOC: headlines:2

The Ultraviolet docket will be debated and voted on at the closing stages of the R7RS Large process to resolve consistency, clarity, fit-and-finish, and similar issues which arise because of the extended, piecewise development of the Large language specification. In lieu of an official list maintained by John Cowan, the chair of the R7RS Large working group, I’m keeping track of them here as they come up. This list is not definitive, official, complete, nor guaranteed to be correct; however, most of the issues on the list have been informally accepted by John as things we’ll need to look at at the Ultraviolet stage.

The best way to make me aware of issues that should be listed here is to ping me about them on the #scheme IRC channel on Liberachat, but I also pay attention to the [[https://srfi.schemers.org/srfi-list-subscribe.html][srfi-discuss mailing list]] and the mailing lists of some, but far from all, new SRFIs. You can also email me personally.

* Current issues

** Fold 

~(scheme vector)~ i.e. [[https://srfi.schemers.org/srfi-133/srfi-133.html][SRFI 133]] ~vector-fold~ passes its arguments to the ~kons~ procedure in the opposite order (accumulator first, then datums from vectors) to ~(scheme list)~ i.e. [[https://srfi.schemers.org/srfi-1/srfi-1.html][SRFI 1]] ~fold~ (datums from lists, then accumulator from list last). As for other folds, these follow the SRFI 1 pattern (the corresponding fold-right procedure can be assumed to behave the same as the left fold):

- ~(scheme bitwise)~ i.e. [[https://srfi.schemers.org/srfi-151/srfi-151.html][SRFI 151]]’s ~bitwise-fold~
- ~(scheme charset)~ i.e. [[https://srfi.schemers.org/srfi-14/srfi-14.html][SRFI 14]]’s ~char-set-fold~
- ~(scheme generator)~ i.e. [[https://srfi.schemers.org/srfi-158/srfi-158.html][SRFI 158]]’s ~generator-fold~
- ~(scheme hash-table)~ i.e. [[https://srfi.schemers.org/srfi-125/srfi-125.html][SRFI 125]]’s ~hash-table-fold~
- ~(scheme ideque)~ i.e. [[https://srfi.schemers.org/srfi-134/srfi-134.html][SRFI 134]]’s ~ideque-fold~
- ~(scheme ilist)~ i.e. [[https://srfi.schemers.org/srfi-116/srfi-116.html][SRFI 116]]’s ~ifold~
- ~(scheme mapping)~ i.e. [[https://srfi.schemers.org/srfi-146/srfi-146.html][SRFI 146]]’s ~mapping-fold~
- ~(scheme regexp)~ i.e. [[https://srfi.schemers.org/srfi-115/srfi-115.html][SRFI 115]]’s ~regexp-fold~
- ~(scheme set)~ i.e. [[https://srfi.schemers.org/srfi-113/srfi-113.html][SRFI 113]]’s ~set-fold~ and ~bag-fold-unique~
- ~(scheme text)~ i.e. [[https://srfi.schemers.org/srfi-135/srfi-135.html][SRFI 135]]’s ~textual-fold~
- ~string-fold~ of all of SRFIs [[https://srfi.schemers.org/srfi-13/srfi-13.html][13]], [[https://srfi.schemers.org/srfi-130/srfi-130.html][130]], [[https://srfi.schemers.org/srfi-140/srfi-140.html][140]], [[https://srfi.schemers.org/srfi-152/srfi-152.html][152]] (insert your own joke here)
- [[https://srfi.schemers.org/srfi-148/srfi-148.html][SRFI 148]] (not yet R7RS’d) ~em-fold~
- [[https://srfi.schemers.org/srfi-179/srfi-179.html][SRFI 179]] (not yet R7RS’d) ~array-fold~
- [[https://srfi.schemers.org/srfi-180/srfi-180.html][SRFI 180]] (not yet R7RS’d) ~json-fold~
- [[https://srfi.schemers.org/srfi-189/srfi-189.html][SRFI 189]] (not yet R7RS’d) ~either-fold~ and ~maybe-fold~
  
And these follow the SRFI 133 pattern:

- ~(scheme stream)~ i.e. [[https://srfi.schemers.org/srfi-41/srfi-41.html][SRFI 41]]’s ~stream-fold~
- ~(scheme vector @)~ i.e. [[https://srfi.schemers.org/srfi-160/srfi-160.html][SRFI 160]]’s ~@vector-fold~
- [[https://srfi.schemers.org/srfi-178/srfi-178.html][SRFI 178]] (not yet R7RS’s) ~bitvector-fold/bool~ and ~bitvector-fold/int~
- [[https://srfi.schemers.org/srfi-196/srfi-196.html][SRFI 196]] (not yet R7RS’d) ~range-fold~
- [[https://srfi.schemers.org/srfi-214/srfi-214.html][SRFI 214]] (not yet R7RS’d) ~flexvector-fold~

This can’t be resolved without breaking compatibility with code that already uses the procedures following one or the other convention (except in cases where ~kons~ is commutative), but clearly the SRFI 1 convention is more common (and ~fold~ itself is probably used more than all the ‘wrong’ versions combined), despite being logically backwards compared to e.g. the arguments to ~apply~ and the way rest arguments to procedures work (gathered at the end, not at the beginning), making it more difficult to write variadic ~kons~ procedures (and to implement those folds in the first place). The SRFI 1 convention is more convenient, though, in the specific case that ~kons~ is literally ~cons~ (i.e. copy a list, or convert from another data type to a list). However, SRFI 1 also provides ~xcons~ as a drop-in replacement for the inverse argument order.

** Arity of folds, maps, etc.

The folds on ideques in ~(scheme ideque)~ i.e. [[https://srfi.schemers.org/srfi-134/srfi-134.html][SRFI 134]] could be variadic, but aren’t. (The folds on unordered types like hash tables could be too, though that’s trickier.) There may be other such cases in other libraries; this needs further investigation.

** Comparators vs comparison predicates

Procedures like ~list=~ from ~(scheme list)~, ~vector=~ from ~(scheme vector)~, etc, take an equality predicate function as their first argument rather than a comparator. Arguably they should take both and automatically unpack the equality predicate from a comparator object when given.

~(scheme sort)~ i.e. [[https://srfi.schemers.org/srfi-132/srfi-132.html][SRFI 132]] takes only ordering predicates rather than comparator objects as the argument to its sorting and merging procedures. Arguably it should likewise take both. This applies to any procedure in any library which takes an ordering predicate rather than a comparator.

~(scheme vector)~ ([[https://srfi.schemers.org/srfi-133/srfi-133.html][SRFI 133]]) has a ~vector-binary-search~ procedure which takes neither, accepting instead a C-style ~cmp~ procedure; likewise [[https://srfi.schemers.org/srfi-214/srfi-214.html][SRFI 214]] ~flexvector-binary-search~. This is completely different from every other comparison function in Scheme. [[https://srfi.schemers.org/srfi-223/][SRFI 223]] proposes a solution, although this also takes only an ordering predicate and not a comparator.

** Mutational and linear-update procedures

There’s an [[https://srfi-email.schemers.org/srfi-discuss/msg/16764585/][issue about what the defined semantics of what Scheme has traditionally called ‘linear-update’ procedures]] (roughly speaking, procedures whose names end in ~!~) over various data structures and various suggestions for how to amend the present definition. Potentially, this affects any library with ‘linear-update’/mutating versions of functional procedures. (At present, ~(define map! map)~ is a completely valid implementation of SRFI 1 ~map!~, since mutation is only allowed, not guaranteed.)

** Naming issues

The name of the ~(scheme charset)~ library doesn’t match the names of the actual procedures in that library, which all start with ~char-set~ with a hyphen.

The name of the ~(scheme regex)~ library likewise doesn’t match the names of the actual procedures in the library, which use ~regexp~ and not ~regex~.

* Potential future issues

** Redundancy of ~(scheme text)~

Adopting [[https://srfi.schemers.org/srfi-140/srfi-140.html][SRFI 140]] would make the majority of Scheme strings immutable and give guaranteed O(1) ~string-ref~ performance on them, making the guarantees of the separate immutable text data type in ~(scheme text)~ ([[https://srfi.schemers.org/srfi-135/srfi-135.html][SRFI 135]]) redundant. It may also be redundant if a variant of SRFI 140 is adopted which retains the property that most Scheme strings are mutable.

** Keyword arguments

If we happen to accept any proposals which use some specific form of keyword arguments before the keyword argument issue is resolved, we should consider changing those libraries to use the standard R7RS keyword argument system if they use a different one, or respecify them not to use keyword arguments at all (which could be tricky in some cases) if we rejected keyword arguments entirely.

** Naming conventions

- If [[https://srfi.schemers.org/srfi-214/srfi-214.html][SRFI 214]] is accepted, its use of a procedure called ~flexvector=?~ does not match the convention established by [[https://srfi.schemers.org/srfi-1/srfi-1.html][SRFI 1]] (i.e. ~(scheme list)~) ~list=~ and [[https://srfi.schemers.org/srfi-133/srfi-133.html][SRFI 133]] (i.e. ~(scheme vector)~) ~vector=~ that comparison predicates taking an additional comparison procedure argument have names that do not end in ~?~.
- There’s arguably a convention that data types whose names have a prefix ‘i’ are immutable. But [[https://srfi.schemers.org/srfi-217/srfi-217.html][SRFI 217]] has ‘iset’ meaning integer set, not immutable set.
