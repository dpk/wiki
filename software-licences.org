#+TITLE: Software licences

** A plea for European software licences

Many FOSS licences around today either explicitly or implicitly assume US jurisdiction.[fn:shouting] For some licences, this is no problem, as their terms can pretty much just as easily be applied in any jurisdiction. This is especially the case with very simple, permissive licences like the [[https://opensource.org/licenses/MIT][MIT licence]] and its [[https://writing.kemitchell.com/2019/03/09/Deprecation-Notice.html][would-be replacement,]] the [[https://blueoakcouncil.org/license/1.0.0][Blue Oak Model Licence]]. But the [[https://opensource.org/licenses/RPL-1.5][Reciprocal Public Licence]] explicitly defines a derivative work by reference to US copyright law, implying it’s intended for use by US-based program authors.

[fn:shouting] This is why so many of them contain paragraphs with ALL CAPS SHOUTING. Some US courts decided at some point that certain clauses in contracts have to be ‘conspicuous’ in order to have effect; pea-brained lawyers then took that and thought they had to write everything in all caps to make it ‘conspicuous’. In Europe, our televisions don’t shout at us, and neither do our contracts.


In fact there’s only one major software licence that explicitly targets European copyright law, which is the [[https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12][EUPL]]. The EUPL is AGPL-like and, for program authors based in the EU, guarantees that the jurisdiction is that of their own country of residence. (Everyone else had better like Belgium.) But has a big loophole: if someone wants to use an EUPL project in their own software, but their own software is has a slightly more permissive licence that doesn’t include the AGPL-like terms, they can actually relicence /your/ software to match /their/ licence. (All the allowed licences for this relicensing are copyleft — but even the LGPL is in the list.)

One could work around this by saying something like ‘Licensed under the EUPL, except that the Compatibility Clause in section 5 does not apply’, like the numerous little footnotes people add to the GPL and GFDL in some projects (linking exceptions, font exceptions, front- and back-cover text exceptions …). But then your project can’t be distributed as a linked part of some other larger software /at all/ unless that other software is also under the EUPL — a tricky problem for anyone wanting to reuse your code who doesn’t live in the EU.

So this loophole in the EUPL should be fixed by saying that redistributing in the context of a larger application is allowed if the larger application is under another licence, /but/ the EUPL continues to apply to the original project itself.

But more generally, it’d be good to have a version of the RPL, for instance, which is more European,[fn:rpl] as well as an EUPL with no Affero-like rules. Even an EU-tuned version of the Blue Oak licence probably wouldn’t go amiss.

[fn:rpl] The RPL is like the AGPL, only more so.
