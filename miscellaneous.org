#+TITLE: Miscellaneous

I don't like having a page of miscellaneous pages. Ideally I should find
ways to link these pages into the rest of the [[file:Wiki-building][wiki
fibre]].

- [[file:retrographics.org][Retrographics]]
- [[file:creativity.org][Creativity]]
- [[file:project-rabbit-hole.org][Project rabbit-holes]]
- [[file:world-wide-web.org][World Wide Web]]
- [[file:recipe-book.org][Recipe books]]
- [[file:free-fonts.org][Free fonts]]
